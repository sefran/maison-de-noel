#include "Volume3.h" // The Volume songs library

unsigned int litrythme = 0; 
unsigned long pulsemusique = 0;

// Tempo à jouer
void setTempoMusique(unsigned long tempo) {
  pulsemusique = 1800/tempo;
}

// Melodie à jouer
// Mon beau sapin
void joueMusiqueMonBeauSapin(int hautparleur, unsigned int note) {
  if (mode_son) {
    vol.tone(hautparleur, MonBeauSapin[note].Octave, MonBeauSapin[note].Nuance*64);
  } else {
    vol.tone(hautparleur, 0, 0);
  }
}

StructDuree joue_musique_monbeausapin(bool marche) {
  StructDuree resultat;
  resultat.temps=MonBeauSapin[litmelodie].Duree*pulsemusique;
  if (marche) {
    joueMusiqueMonBeauSapin(hautparleur1, litmelodie);
  } else {
    joue_musique_monbeausapin_stop();
  }
  litmelodie++;
  if (litmelodie>sizeof(MonBeauSapin)/sizeof(StructNote)) {
    joue_musique_monbeausapin_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_musique_monbeausapin_stop() {
  Serial.println("Musique Fin");
  vol.noTone();
  litmelodie = 0;
  joue_musique_monbeausapin_on = false;
}

void joue_musique_monbeausapin_start() {
  Serial.println("Musique");
  joue_musique_monbeausapin_on = true;
}

bool musique_monbeausapin_marche(bool marche) {
  setTempoMusique(tempoMonbeausapin);
  noteDuree = joue_musique_monbeausapin(musique_monbeausapin_on);
  attentemusique = referencetemps + noteDuree.temps;
  if(!noteDuree.joue) {
    musique_monbeausapin_stop();
    return false;
  }
  return marche;
}

void musique_monbeausapin_stop() {
  Serial.println("Musique MonBeauSapin Fin");
  attentemusique = 0;
  musique_monbeausapin_on = false;
}

void musique_monbeausapin_start() {
  Serial.println("Musique MonBeauSapin");
  joue_musique_monbeausapin_start();
  musique_monbeausapin_on = true;
}

// Rythme à jouer
void joueRythmeMonBeauSapin(int hautparleur, unsigned int note) {
  if (RythmeMonBeauSapin[note].Frequence!=0) {
    tone(hautparleur, RythmeMonBeauSapin[note].Frequence, RythmeMonBeauSapin[note].Duree);
  } else {
    tone(hautparleur, 0, 0);
  }
}

StructDuree joue_rythme_monbeausapin(bool marche) {
  StructDuree resultat;
  resultat.temps=RythmeMonBeauSapin[litrythme].Duree*pulsemusique;
  if (marche && mode_son) {
    joueRythmeMonBeauSapin(hautparleur2, litrythme);
  } else {
    joue_rythme_monbeausapin_stop();
  }
  litrythme++;
  if (litrythme>sizeof(RythmeMonBeauSapin)/sizeof(StructRythme)) {
    joue_rythme_monbeausapin_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_rythme_monbeausapin_stop() {
  Serial.println("Rythme Fin");
  noTone(hautparleur2);
  litrythme = 0;
  joue_rythme_monbeausapin_on = false;
}

void joue_rythme_monbeausapin_start() {
  Serial.println("Rythme");
  joue_rythme_monbeausapin_on = true;
}

bool rythme_monbeausapin_marche(bool marche) {
      rythmeDuree = joue_rythme_monbeausapin(rythme_monbeausapin_on);
      attenterythme = referencetemps + rythmeDuree.temps;
      if (!rythmeDuree.joue) {
        rythme_monbeausapin_stop();
        return false;
      }
      return marche;
}

void rythme_monbeausapin_stop() {
  Serial.println("Rythme MonBeauSapin Fin");
  joue_rythme_monbeausapin_stop();
  attenterythme = 0;
  rythme_monbeausapin_on = false;
}

void rythme_monbeausapin_start() {
  Serial.println("Rythme MonBeauSapin");
  joue_rythme_monbeausapin_start();
  rythme_monbeausapin_on = true;
}

// Jingle bells
void joueMusiqueJingleBells(int hautparleur, unsigned int note) {
  if (mode_son) {
    vol.tone(hautparleur, JingleBells[note].Octave, JingleBells[note].Nuance*64);
  } else {
    vol.tone(hautparleur, 0, 0);
  }
}

StructDuree joue_musique_jinglebells(bool marche) {
  StructDuree resultat;
  resultat.temps=JingleBells[litmelodie].Duree*pulsemusique;
  if (marche) {
    joueMusiqueJingleBells(hautparleur1, litmelodie);
  } else {
    joue_musique_jinglebells_stop();
  }
  litmelodie++;
  if (litmelodie>sizeof(JingleBells)/sizeof(StructNote)) {
    joue_musique_jinglebells_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_musique_jinglebells_stop() {
  Serial.println("Musique Fin");
  vol.noTone();
  litmelodie = 0;
  joue_musique_jinglebells_on = false;
}

void joue_musique_jinglebells_start() {
  Serial.println("Musique");
  joue_musique_jinglebells_on = true;
}

bool musique_jinglebells_marche(bool marche) {
  setTempoMusique(tempoJingleBells);
  noteDuree = joue_musique_jinglebells(musique_jinglebells_on);
  attentemusique = referencetemps + noteDuree.temps;
  if(!noteDuree.joue) {
    musique_jinglebells_stop();
    return false;
  }
  return marche;
}

void musique_jinglebells_stop() {
  Serial.println("Musique Jinglebells Fin");
  attentemusique = 0;
  musique_jinglebells_on = false;
}

void musique_jinglebells_start() {
  Serial.println("Musique Jinglebells");
  joue_musique_jinglebells_start();
  musique_jinglebells_on = true;
}

// Rythme à jouer
void joueRythmeJingleBells(int hautparleur, unsigned int note) {
  if (RythmeJingleBells[note].Frequence!=0) {
    tone(hautparleur, RythmeJingleBells[note].Frequence, RythmeJingleBells[note].Duree);
  } else {
    tone(hautparleur, 0, 0);
  }
}

StructDuree joue_rythme_jinglebells(bool marche) {
  StructDuree resultat;
  resultat.temps=RythmeJingleBells[litrythme].Duree*pulsemusique;
  if (marche && mode_son) {
    joueRythmeJingleBells(hautparleur2, litrythme);
  } else {
    joue_rythme_jinglebells_stop();
  }
  litrythme++;
  if (litrythme>sizeof(RythmeJingleBells)/sizeof(StructRythme)) {
    joue_rythme_jinglebells_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_rythme_jinglebells_stop() {
  Serial.println("Rythme Fin");
  noTone(hautparleur2);
  litrythme = 0;
  joue_rythme_jinglebells_on = false;
}

void joue_rythme_jinglebells_start() {
  Serial.println("Rythme");
  joue_rythme_jinglebells_on = true;
}

bool rythme_jinglebells_marche(bool marche) {
      rythmeDuree = joue_rythme_jinglebells(rythme_jinglebells_on);
      attenterythme = referencetemps + rythmeDuree.temps;
      if (!rythmeDuree.joue) {
        rythme_jinglebells_stop();
        return false;
      }
      return marche;
}

void rythme_jinglebells_stop() {
  Serial.println("Rythme Jinglebells Fin");
  joue_rythme_jinglebells_stop();
  attenterythme = 0;
  rythme_jinglebells_on = false;
}

void rythme_jinglebells_start() {
  Serial.println("Rythme Jinglebells");
  joue_rythme_jinglebells_start();
  rythme_jinglebells_on = true;
}
