#include "Musique.h" // The Music constants
#include "ConfigProject.h" // Configuration of project

void joueMusiqueMonBeauSapin(int);
void joueRythmeMonBeauSapin(int);
void joueMusiqueJinglebells(int);
void joueRythmeJinglebells(int);
bool chenillard_marche(bool);
void lampadere_marche(bool);
void lampadererandom_marche(bool);

unsigned long referencetemps = 0;
unsigned long oldreferencetemps = 0;

// Lampadère
bool lampadere_on = false;
bool lampadererandom_on = false;
bool eclairesapin = false;
// Gestion du temps lampadère
unsigned long tempslampadere = 500;
unsigned long attentelampadere = 0;

// Leds
bool chenillard_on = true;
bool chenillardstick_on = false;
bool chenillardcolor_on = false;
bool chenillardrandom_on = false;
int maxchenillardrandomcount = randomCount;
// Gestion du temps leds
unsigned long tempsleds = 500;
unsigned long attenteleds = 0;

// Sapin
bool animationsapin_on = false;

// Musique
StructDuree noteDuree;
StructDuree rythmeDuree;
int sensorMusiqueOn = 0;
float voltageMusiqueOn = 0;
bool mode_son = false;
bool joue_musique = false;
bool joue_rythme = false;
bool joue_musique_monbeausapin_on = false;
bool joue_rythme_monbeausapin_on = false;
bool musique_monbeausapin_on = false;
bool rythme_monbeausapin_on = false;
bool joue_musique_jinglebells_on = false;
bool joue_rythme_jinglebells_on = false;
bool musique_jinglebells_on = false;
bool rythme_jinglebells_on = false;
unsigned int litmelodie = 0; // Indice lecture notes musiques
// Gestion du temps musique
unsigned long attentemusique = 0;
unsigned long attenterythme = 0;

// Animation musicale Mon beau sapin
bool animation_monbeausapin_on = false;

// Animation visuelle maison
bool animation_visuelle_maison = false;

// Animation musicale Jingle bells
bool animation_jinglebells_on = false;

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
  init_leds();
  init_sapin(moteurSapin, ledsapin);
  init_lampadere(lampadere);
}

// the loop routine runs over and over again forever:
void loop() {
  referencetemps = millis();

  // dépassement de limites de millis() (remise à zéro) TODO à améliorer
  //if (referencetemps<oldreferencetemps) {
  //  attenteleds = 0;
  //  attentelampadere = 0;
  //  attentemusique = 0;
  //}

  // Detection validation musique
  sensorMusiqueOn = analogRead(A0);
  voltageMusiqueOn = sensorMusiqueOn * (5.0 / 1023.0);
  if (voltageMusiqueOn > 2.5) {
    mode_son = true;
  } else {
    mode_son = false;
  }

  // Chenillard de leds
  if (attenteleds<=referencetemps) {
    attenteleds = referencetemps + tempsleds;
    if (chenillard_on) {
      if (!chenillard_marche(chenillard_on)) {
        // Animation suivante
        if (!animation_visuelle_maison) {
         animation_monbeausapin_start();
        }      
      }
    }
    if (animation_visuelle_maison) {
      if (!animation_visuelle_maison_marche(animation_visuelle_maison)) {
        animation_jinglebells_start();
      }
    }
  }

  // Lampadère
  if (attentelampadere<=referencetemps) {
    attentelampadere = referencetemps + tempslampadere;
    if (lampadere_on) {
      lampadere_marche(lampadere_on);
    }
    if (lampadererandom_on) {
      lampadererandom_marche(lampadererandom_on);
    }
  }

  // Mise en marche animation sapin
  animationsapin_marche(animationsapin_on);

  // Mise en marche animation mon beau sapin
  if (animation_monbeausapin_on) {
    if(!animation_monbeausapin_marche(animation_monbeausapin_on)) {
      // Animation suivante
      animation_monbeausapin_stop();
      animation_visuelle_start();
    }
  }
  
  // Mise en marche animation jingle bells
  if (animation_jinglebells_on) {
    if(!animation_jinglebells_marche(animation_jinglebells_on)) {
      // Animation suivante
      animation_jinglebells_stop();
      chenillard_start();
    }
  }
  
  // Mise en marche musique
  if (attentemusique<=referencetemps) {
    if (musique_monbeausapin_on) {
      if(!musique_monbeausapin_marche(musique_monbeausapin_on)) {
        joue_musique = false;
      }
    }
    if (musique_jinglebells_on) {
      chenillardrandom_marche(chenillardrandom_on);
      if(!musique_jinglebells_marche(musique_jinglebells_on)) {
        joue_musique = false;
      }
    }
  }

  // Mise en marche rythme
  if (attenterythme<=referencetemps) {
    if (rythme_monbeausapin_on) {
      if(!rythme_monbeausapin_marche(rythme_monbeausapin_on)) {
        joue_rythme = false;
      }
    }
    if (rythme_jinglebells_on) {
      if(!rythme_jinglebells_marche(rythme_jinglebells_on)) {
        joue_rythme = false;
      }
    }
  }

  //oldreferencetemps=referencetemps;    
}
