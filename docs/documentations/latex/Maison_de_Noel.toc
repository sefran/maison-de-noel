\babel@toc {french}{}\relax 
\contentsline {chapter}{\numberline {1}Projet}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Le Matériel de départ}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Les améliorations électroniques}{7}{section.1.2}%
\contentsline {chapter}{\numberline {2}La réalisation}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}Les tests}{11}{section.2.1}%
\contentsline {section}{\numberline {2.2}L’électronique}{14}{section.2.2}%
\contentsline {section}{\numberline {2.3}Le montage}{15}{section.2.3}%
\contentsline {section}{\numberline {2.4}Le programme}{21}{section.2.4}%
\contentsline {section}{\numberline {2.5}Améliorations possibles}{49}{section.2.5}%
\contentsline {section}{\numberline {2.6}Le résultat final}{49}{section.2.6}%
