.. only:: not latex

  ############################################################################
  `Projet Maison de Noël numérique <https://sefran.frama.io/maison-de-noel/>`_
  ############################################################################
  
.. only:: latex
  
  ###############################
  Projet Maison de Noël numérique
  ###############################
  
  `Voir ce projet en ligne <https://sefran.frama.io/maison-de-noel/>`_
  
Projet
******
  
Je me suis lancé en Décembre 2022 dans un projet électronique de numérisation d'un village de Noël. Voici la documentation de la réalisation de ce projet.
  
  
Le Matériel de départ
=====================

C'est un village de Noël équipée de LEDs qui s'éclairent en permanence, avec un sapin de Noel en movement propulsé par un moteur.
Il a un lampadaire et un moulin à eau. 
  
.. image:: ./images/VillageDeNoel.jpg
  :align: center
  :width: 680px
  :alt: Le village de Noël vu de face
  

.. image:: ./images/VillageDeNoel_dos.jpg
  :align: center
  :width: 680px
  :alt: Le village de Noël vu de dos


Il est accessible par le fond :
  
.. image:: ./images/0_Départ_Fond.jpg
  :align: center
  :width: 680px
  :alt: Le fond du village de Noël
  

Lorsque on consulte son contenu électrique intérieur, il est constitué :

* d'une prise d'alimentation genre DC005,
* d'un interrupteur coulissant.

  .. image:: ./images/0_Alimentation_Interupteur0.jpg
    :align: center
    :width: 680px
    :alt: L'interrupteur coulissant et le connecteur DC005 montés vu de l'extérieur


  .. image:: ./images/0_Alimentation_Interupteur1.jpg
    :align: center
    :width: 680px
    :alt: L'interrupteur coulissant et le connecteur DC005


Et son câblage intérieur est très sommaire :
  
.. image:: ./images/0_Cablage.jpg
  :align: center
  :width: 680px
  :alt: Câblage du village de Noël


Les améliorations électroniques
===============================

Ajout d'une alimentation à découpage pour avoir une tension de sortie de 5V pour l'électronique, la possibilité de tensions d'alimentations d'entrées variables.

.. image:: ./images/1_Ajout_Alimentation.jpg
  :align: center
  :width: 680px
  :alt: Alimentation DC-DC 5V

Ajout de deux haut parleurs pour pouvoir jouer des sons sur deux voix.

.. image:: ./images/2_Ajout_HautPaleurs.jpg
  :align: center
  :width: 680px
  :alt: Hauts parleurs

Ajout d'un interrupteur pour pouvoir couper la musique.

.. image:: ./images/3_InterrupteurSon.jpg
  :align: center
  :width: 680px
  :alt: Interrupteur coulissant

Ajout d'une masse électrique commune.

.. image:: ./images/4_Masse_Electrique.jpg
  :align: center
  :width: 100 %
  :alt: Connecteur masses électrique des composants électroniques

Ajout d'un microcontrôleur pour animer tout cela.

.. image:: ./images/5_Ajout_ArduinoNano.jpg
  :align: center
  :width: 680px
  :alt: Arduino Nano du montage


La réalisation
**************

Les tests
=========

D'abord, j'ai commencé à vouloir numériser le village de Noël avec un ESP 8266.

.. image:: ./images/Test_ESP8266.jpg
  :align: center
  :width: 680px
  :alt: L'esp8266 connecté


Et je me suis retrouvé en difficulté avec la puissance électrique de sortie limité à 12mA avec le moteur du sapin.

J'ai donc fait un test avec un montage de commande à base MOSFET IFRZ44N.

.. image:: ./images/Test_AlimentationMoteurSapin.jpg
  :align: center
  :width: 100 %
  :alt: Montage d'alimentation du moteur du sapin


Le montage a fonctionné.

J'avais assez de PIO pour faire fonctionner les LEDs (rajout possible d'une extension I2C). 

Mais je me suis trouvé bloquer avec les deux haut parleurs et le générateur d'impulsions.

Je suis donc passé avec un Arduino Nano. Celui-ci étant limité en courant à 40mA, et le moteur consommant 36mA, j'ai pu enlever l'interface MOSFET.


L'électronique
==============

.. image:: ./images/nano.jpg
  :align: center
  :width: 680px
  :alt: Schéma des sorties Arduino Nano


.. image:: ./images/VillageDeNoel_Electronique.png
  :align: center
  :width: 680px
  :alt: Montage électronique


Le montage
==========

Branchement de l'alimentation à découpage.

.. image:: ./images/1_Ajout_Alimentation.jpg
  :align: center
  :width: 680px
  :alt: Alimentation à découpage


Mise en place du connecteur d'alimentation.

.. image:: ./images/6_BranchementAlimentation.jpg
  :align: center
  :width: 680px
  :alt: Alimentation du village de Noël


Raccordement de l'alimentation au microcontrôleur.

.. image:: ./images/6_ConnectionAlimentation.jpg
  :align: center
  :width: 680px
  :alt: Alimentation Arduino Nano


Raccordement des leds.

.. image:: ./images/7_ConnectionLeds.jpg
  :align: center
  :width: 680px
  :alt: Alimentation LEDs


Protection du microcontrôleur contre les surintensités de la self du moteur du sapin.

.. image:: ./images/8_ProtectionSelfMoteur.jpg
  :align: center
  :width: 680px
  :alt: Protection moteur


Raccordement de l'alimentation du moteur du sapin.

.. image:: ./images/8_AlimentationEtConnectionMoteurSapin.jpg
  :align: center
  :width: 680px
  :alt: Alimentation et connection moteur sapin


Raccordement des haut parleurs.

.. image:: ./images/9_ConnectionHautParleurs.jpg
  :align: center
  :width: 680px
  :alt: Connexion Hauts parleurs


Raccordement de l'interrupteur pour couper la musique.

.. image:: ./images/10_ConnectionInterrupteurSon.jpg
  :align: center
  :width: 680px
  :alt: Connection interrupteur musique


Le programme
============

Le programme est constitué de 7 fichiers.

Le fichier d'entrée maison-de-noel.ino

.. literalinclude:: ../../maison-de-noel.ino
  :language: c
  :linenos:
    

Le fichier de configuration du projet ConfigProject.h

.. literalinclude:: ../../ConfigProject.h
  :language: c
  :linenos:


Le fichier de paramètres musicaux Musique.h

.. literalinclude:: ../../Musique.h
  :language: c
  :linenos:


Le fichier des musiques de Noël et de leurs contrôles MusiquesNoel.ino

.. literalinclude:: ../../MusiquesNoel.ino
  :language: c
  :linenos:
  

Le fichier de contrôle du sapin Sapin.ino

.. literalinclude:: ../../Sapin.ino
  :language: c
  :linenos:


Le fichier de commandes des éclairages Eclairages.ino

.. literalinclude:: ../../Eclairages.ino
  :language: c
  :linenos:


Le fichiers des animations du village de Noël Animations.ino

.. literalinclude:: ../../Animations.ino
  :language: c
  :linenos:


Améliorations possibles
=======================

La mémoire limitée de l’Arduino Nano limite les animations possibles.

Le montage est prévu pour pouvoir évoluer avec une EEPROM en i2c sur les pattes A4 et A5 de l’Arduino Nano.

Les musiques peuvent alors être stockées dans une EEPROM, ce qui permettrait de récupérer pas mal de ressources pour le microcontrôleur. Il faut alors modifier ce programme pour lire les musiques à partir de l'EEPROM, et créer un autre programme pour écrire les musiques dans une EEPROM.

Peut-être une V2 pour l'année prochaine ?


Le résultat final
=================

`Voir sur Youtube <https://youtu.be/Mbh5EpDlPqY>`_

