.. |date| date::

:Date: |date|
:Revision: |version|
:Author: FrancSERRES <sefran007@gmail.com>
:Description: Documentation numérisation d'une maison de Noël
:Info: Voir <https://sefran.frama.io/maison-de-noel/> pour la mise à jour de cette documentation.

.. toctree::
   :maxdepth: 3
   :caption: Contenu
   

.. include:: MaisonDeNoel.rst

