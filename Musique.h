#include <EEPROM.h>

// Définition des fréquences des octaves de musiques
#define OCTAVE_1_DO 33
#define OCTAVE_1_DOd 35
#define OCTAVE_1_RE 37
#define OCTAVE_1_REd 39
#define OCTAVE_1_MI 41
#define OCTAVE_1_FA 44
#define OCTAVE_1_FAd 46
#define OCTAVE_1_SOL 49
#define OCTAVE_1_SOLd 52
#define OCTAVE_1_LA 55
#define OCTAVE_1_SIb 58
#define OCTAVE_1_SI 62
#define OCTAVE_2_DO 65
#define OCTAVE_2_DOd 69
#define OCTAVE_2_RE 73
#define OCTAVE_2_REd 78
#define OCTAVE_2_MI 82
#define OCTAVE_2_FA 87
#define OCTAVE_2_FAd 92
#define OCTAVE_2_SOL 98
#define OCTAVE_2_SOLd 104
#define OCTAVE_2_LA 110
#define OCTAVE_2_SIb 117
#define OCTAVE_2_SI 123
#define OCTAVE_3_DO 131
#define OCTAVE_3_DOd 139
#define OCTAVE_3_RE 157
#define OCTAVE_3_REd 156
#define OCTAVE_3_MI 165
#define OCTAVE_3_FA 175
#define OCTAVE_3_FAd 185
#define OCTAVE_3_SOL 196
#define OCTAVE_3_SOLd 208
#define OCTAVE_3_LA 220
#define OCTAVE_3_SIb 233
#define OCTAVE_3_SI 247
#define OCTAVE_4_DO 262
#define OCTAVE_4_DOd 277
#define OCTAVE_4_RE 294
#define OCTAVE_4_REd 311
#define OCTAVE_4_MI 330
#define OCTAVE_4_FA 349
#define OCTAVE_4_FAd 370
#define OCTAVE_4_SOL 392
#define OCTAVE_4_SOLd 415
#define OCTAVE_4_LA 440
#define OCTAVE_4_SIb 466
#define OCTAVE_4_SI 494
#define OCTAVE_5_DO 523
#define OCTAVE_5_DOd 554
#define OCTAVE_5_RE 587
#define OCTAVE_5_REd 622
#define OCTAVE_5_MI 659
#define OCTAVE_5_FA 698
#define OCTAVE_5_FAd 740
#define OCTAVE_5_SOL 784
#define OCTAVE_5_SOLd 831
#define OCTAVE_5_LA 880
#define OCTAVE_5_SIb 932
#define OCTAVE_5_SI 988
#define OCTAVE_6_DO 1047
#define OCTAVE_6_DOd 1109
#define OCTAVE_6_RE 1175
#define OCTAVE_6_REd 1245
#define OCTAVE_6_MI 1319
#define OCTAVE_6_FA 1397
#define OCTAVE_6_FAd 1480
#define OCTAVE_6_SOL 1568
#define OCTAVE_6_SOLd 1661
#define OCTAVE_6_LA 1760
#define OCTAVE_6_SIb 1865
#define OCTAVE_6_SI 1976
#define OCTAVE_7_DO 2093
#define OCTAVE_7_DOd 2217
#define OCTAVE_7_RE 2349
#define OCTAVE_7_REd 2489
#define OCTAVE_7_MI 2637
#define OCTAVE_7_FA 2794
#define OCTAVE_7_FAd 2960
#define OCTAVE_7_SOL 3136
#define OCTAVE_7_SOLd 3322
#define OCTAVE_7_LA 3520
#define OCTAVE_7_SIb 3729
#define OCTAVE_7_SI 3951
#define OCTAVE_8_DO 4186
#define OCTAVE_8_DOd 4435
#define OCTAVE_8_RE 4699
#define OCTAVE_8_REd 4978
#define OCTAVE_8_MI 5274
#define OCTAVE_8_FA 5588
#define OCTAVE_8_FAd 5920
#define OCTAVE_8_SOL 6272
#define OCTAVE_8_SOLd 6645
#define OCTAVE_8_LA 7040
#define OCTAVE_8_SIb 7459
#define OCTAVE_8_SI 7902
#define OCTAVE_9_DO 8372
#define OCTAVE_9_DOd 8870
#define OCTAVE_9_RE 9397
#define OCTAVE_9_REd 9956
#define OCTAVE_9_MI 10548
#define OCTAVE_9_FA 11175
#define OCTAVE_9_FAd 11840
#define OCTAVE_9_SOL 12544
#define OCTAVE_9_SOLd 13290
#define OCTAVE_9_LA 14080
#define OCTAVE_9_SIb 14917
#define OCTAVE_9_SI 15804

// Silences
#define SILENCE_32eme_soupir 1 // 1/32 de pulsation
#define SILENCE_16eme_soupir 2
#define SILENCE_8eme_soupir 4
#define SILENCE_quart_soupir 8
#define SILENCE_demi_soupir 16
#define SILENCE_soupir 32
#define SILENCE_demi_pause 64
#define SILENCE_pause 128
#define SILENCE_baton_pause 256

// Temps
#define DUREE_quintuple_croche 1 // 1/32 de pulsation
#define DUREE_quadruple_croche 2
#define DUREE_quadruple_croche_pointee 3
#define DUREE_triple_croche 4
#define DUREE_triple_croche_pointee 6
#define DUREE_double_croche 8
#define DUREE_double_croche_pointee 12
#define DUREE_croche 16
#define DUREE_croche_pointee 24
#define DUREE_noire 32
#define DUREE_noire_pointee 48
#define DUREE_blanche 64
#define DUREE_blanche_pointee 96
#define DUREE_ronde 128
#define DUREE_ronde_pointee 192
#define DUREE_carree 256
#define DUREE_carree_pointee 384

// Tempo
#define TEMPO_larghissimo 40 // pulsation par minutes
#define TEMPO_largo 45
#define TEMPO_larghetto 50
#define TEMPO_lentissimo 55
#define TEMPO_lento 60
#define TEMPO_adagiosissimo 65
#define TEMPO_adagissimo 70
#define TEMPO_adagio 75
#define TEMPO_adagietto 80
#define TEMPO_andante 85
#define TEMPO_andantino 90
#define TEMPO_moderato 95
#define TEMPO_moderato2 100
#define TEMPO_allegretto 110
#define TEMPO_allegro 120
#define TEMPO_vivace 130
#define TEMPO_vivace2 140
#define TEMPO_presto 150
#define TEMPO_presto2 160
#define TEMPO_presto3 170
#define TEMPO_prestissimo 180
#define TEMPO_prestissimo2 190
#define TEMPO_prestissimo3 200

typedef struct
{
  bool joue;
  unsigned long temps;
} StructDuree;

typedef struct
{
  unsigned int Duree;
  unsigned int Octave;
  unsigned int Nuance;
} StructNote;

typedef struct
{
  unsigned int Duree;
  unsigned int Frequence;
} StructRythme;

// Melodie à jouer
static StructNote MonBeauSapin[] = {
  {DUREE_noire, OCTAVE_3_DO, 1},
  {DUREE_croche_pointee, OCTAVE_3_FA, 2},
  {DUREE_double_croche, OCTAVE_3_FA, 3},
  {DUREE_blanche, OCTAVE_3_FA, 4},
  {DUREE_noire, OCTAVE_3_SOL, 5},
  {DUREE_croche_pointee, OCTAVE_3_LA, 6},
  {DUREE_double_croche, OCTAVE_3_LA, 7},
  {DUREE_noire_pointee, OCTAVE_3_LA, 8},
  {DUREE_croche, OCTAVE_3_FA, 9},
  {DUREE_croche, OCTAVE_3_SOL, 10},
  {DUREE_croche, OCTAVE_3_LA, 11},
  {DUREE_noire, OCTAVE_3_SI, 12},
  {DUREE_noire, OCTAVE_3_MI, 13},
  {DUREE_noire, OCTAVE_3_SOL, 14},
  {DUREE_noire, OCTAVE_3_FA, 15},
  {SILENCE_demi_soupir, 0, 0},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_noire_pointee, OCTAVE_4_RE, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_noire, OCTAVE_3_SI, 16},
  {SILENCE_demi_soupir, 0, 0},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_blanche, OCTAVE_3_LA, 16},
  {DUREE_noire, OCTAVE_3_DO, 16},
  {DUREE_croche_pointee, OCTAVE_3_FA, 16},
  {DUREE_double_croche, OCTAVE_3_FA, 16},
  {DUREE_blanche, OCTAVE_3_FA, 16},
  {DUREE_noire, OCTAVE_3_SOL, 16},
  {DUREE_croche_pointee, OCTAVE_3_LA, 16},
  {DUREE_double_croche, OCTAVE_3_LA, 16},
  {DUREE_noire_pointee, OCTAVE_3_LA, 16},
  {DUREE_croche, OCTAVE_3_FA, 16},
  {DUREE_croche, OCTAVE_3_SOL, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_noire, OCTAVE_3_SI, 16},
  {DUREE_noire, OCTAVE_3_MI, 16},
  {DUREE_noire, OCTAVE_3_SOL, 16},
  {DUREE_noire, OCTAVE_3_FA, 16},
};

// Rythme
static StructRythme RythmeMonBeauSapin[] {
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_1_SI},
  {DUREE_blanche, OCTAVE_2_DO},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_DO},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_noire, OCTAVE_2_MI},
  {DUREE_noire, OCTAVE_1_LA},
  {DUREE_noire, OCTAVE_2_MI},
  {SILENCE_demi_pause, 0},
};

// Melodie à jouer
static StructNote JingleBells[] = {
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_LA, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  //
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_6_DO, 16},
  {DUREE_croche, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire_pointee, OCTAVE_6_FA, 16},
  {DUREE_croche, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_RE, 16},
  {DUREE_blanche, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_6_DO, 16},
  {DUREE_croche, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire_pointee, OCTAVE_6_FA, 16},
  {DUREE_croche, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_DO, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire_pointee, OCTAVE_6_DO, 16},
  //{DUREE_croche, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire_pointee, OCTAVE_6_FA, 16},
  //{DUREE_croche, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_RE, 16},
  //{DUREE_blanche, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire_pointee, OCTAVE_6_DO, 16},
  //{DUREE_croche, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire_pointee, OCTAVE_6_FA, 16},
  //{DUREE_croche, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_DO, 16},
};

// Rythme
static StructRythme RythmeJingleBells[] {
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SOL},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_LA},
  {DUREE_noire, OCTAVE_3_LA},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SI},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SOL},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_LA},
  {DUREE_noire, OCTAVE_3_LA},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_ronde, OCTAVE_4_DO},
  //
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
};
