void animationsapin_stop() {
  Serial.println("Animation Sapin Fin");
  ledsapin_stop();
  moteursapin_stop();
  animationsapin_on = false;
}

void animationsapin_start() {
  Serial.println("Animation Sapin");
  ledsapin_start();
  moteursapin_start();
  animationsapin_on = true;
}

bool animation_monbeausapin_marche(bool marche) {
  if(!joue_musique) {
    return false;
  }
  return marche;
}

bool animation_jinglebells_marche(bool marche) {
  if(!joue_musique) {
    return false;
  }
  return marche;
}

void animation_visuelle_stop() {
  Serial.println("Animation visuelle Fin");
  //lampadererandom_stop();
  animationsapin_stop();
  animation_visuelle_maison = false;
}

void animation_visuelle_start() {
  Serial.println("Animation visuelle");
  //lampadererandom_start();
  animationsapin_start();
  chenillardstick_start();
  animation_visuelle_maison = true;  
}

void animation_monbeausapin_stop() {
  Serial.println("Animation MonBeauSapin Fin");
  lampadere_stop();
  animationsapin_stop();
  musique_monbeausapin_stop();
  rythme_monbeausapin_stop();
  animation_monbeausapin_on = false;
}

void animation_monbeausapin_start() {
  Serial.println("Animation MonBeauSapin");
  lampadere_start();
  animationsapin_start();
  musique_monbeausapin_start();
  rythme_monbeausapin_start();
  animation_monbeausapin_on = true;
  joue_musique = true;
  joue_rythme = true;
}

void animation_jinglebells_stop() {
  Serial.println("Animation Jinglebells Fin");
  chenillardrandom_stop();
  musique_jinglebells_stop();
  rythme_jinglebells_stop();
  animation_jinglebells_on = false;
}

void animation_jinglebells_start() {
  Serial.println("Animation Jinglebells");
  chenillardrandom_start();
  musique_jinglebells_start();
  rythme_jinglebells_start();
  animation_jinglebells_on = true;
  joue_musique = true;
  joue_rythme = true; 
}
