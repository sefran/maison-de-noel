bool sapin_on = false;

void init_sapin(int moteur_sapin, int led_sapin) {
  // declare pin to be an output:
  pinMode(led_sapin, OUTPUT);
  pinMode(moteurSapin, OUTPUT);

  ledsapin_stop();
  moteursapin_stop();
}

void moteursapin_marche(bool marche) {
  if(marche && sapin_on) {
    digitalWrite(moteurSapin, LOW);
  } else {
    digitalWrite(moteurSapin, HIGH); 
  }  
}

void moteursapin_stop() {
  Serial.println("Moteur Sapin Fin");
  digitalWrite(moteurSapin, HIGH);
  sapin_on = false;
}

void moteursapin_start() {
  Serial.println("Moteur Sapin");
  sapin_on = true;  
}

void animationsapin_marche(bool marche) {
  ledsapin_marche(marche);
  moteursapin_marche(marche);
}
