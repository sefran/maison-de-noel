// Définition périphériques
#define ledsapin 2
#define lampadere 3 // the PWM pin the LED is attached to
#define ledRouge1 4 //porte entrée
#define ledRouge2 5 // PWM possible 
#define ledVerte1 6 // PWM possible
#define ledVerte2 7
#define ledbleu1 8
#define moteurSapin 9 // the PWM motor
#define hautparleur1 10 // the PWM HP1
#define hautparleur2 11 // the PWM HP2
#define ledBleu2 12
#define randomCount 100 // Max time random
#define tempoMonbeausapin TEMPO_adagio
#define tempoJingleBells TEMPO_prestissimo
 
