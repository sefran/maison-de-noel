# [Projet Maison de Noël numérique](https://sefran.frama.io/maison-de-noel/)

# Projet

Je me suis lancé en Décembre 2022 dans un projet électronique de numérisation d’un village de Noël. Voici la documentation de la réalisation de ce projet.

## Le Matériel de départ

C’est un village de Noël équipée de LEDs qui s’éclairent en permanence, avec un sapin de Noel en movement propulsé par un moteur.
Il a un lampadaire et un moulin à eau.



![image](docs/sources-documents/images/VillageDeNoel.jpg)



![image](docs/sources-documents/images/VillageDeNoel_dos.jpg)

Il est accessible par le fond :



![image](docs/sources-documents/images/0_Départ_Fond.jpg)

Lorsque on consulte son contenu électrique intérieur, il est constitué :


* d’une prise d’alimentation genre DC005,


* d’un interrupteur coulissant.



![image](docs/sources-documents/images/0_Alimentation_Interupteur0.jpg)



![image](docs/sources-documents/images/0_Alimentation_Interupteur1.jpg)

Et son câblage intérieur est très sommaire :



![image](docs/sources-documents/images/0_Cablage.jpg)

## Les améliorations électroniques

Ajout d’une alimentation à découpage pour avoir une tension de sortie de 5V pour l’électronique, la possibilité de tensions d’alimentations d’entrées variables.



![image](docs/sources-documents/images/1_Ajout_Alimentation.jpg)

Ajout de deux haut parleurs pour pouvoir jouer des sons sur deux voix.



![image](docs/sources-documents/images/2_Ajout_HautPaleurs.jpg)

Ajout d’un interrupteur pour pouvoir couper la musique.



![image](docs/sources-documents/images/3_InterrupteurSon.jpg)

Ajout d’une masse électrique commune.



![image](docs/sources-documents/images/4_Masse_Electrique.jpg)

Ajout d’un microcontrôleur pour animer tout cela.



![image](docs/sources-documents/images/5_Ajout_ArduinoNano.jpg)

# La réalisation

## Les tests

D’abord, j’ai commencé à vouloir numériser le village de Noël avec un ESP 8266.



![image](docs/sources-documents/images/Test_ESP8266.jpg)

Et je me suis retrouvé en difficulté avec la puissance électrique de sortie limité à 12mA avec le moteur du sapin.

J’ai donc fait un test avec un montage de commande à base MOSFET IFRZ44N.



![image](docs/sources-documents/images/Test_AlimentationMoteurSapin.jpg)

Le montage a fonctionné.

J’avais assez de PIO pour faire fonctionner les LEDs (rajout possible d’une extension I2C).

Mais je me suis trouvé bloquer avec les deux haut parleurs et le générateur d’impulsions.

Je suis donc passé avec un Arduino Nano. Celui-ci étant limité en courant à 40mA, et le moteur consommant 36mA, j’ai pu enlever l’interface MOSFET.

## L’électronique



![image](docs/sources-documents/images/nano.jpg)



![image](docs/sources-documents/images/VillageDeNoel_Electronique.png)

## Le montage

Branchement de l’alimentation à découpage.



![image](docs/sources-documents/images/1_Ajout_Alimentation.jpg)

Mise en place du connecteur d’alimentation.



![image](docs/sources-documents/images/6_BranchementAlimentation.jpg)

Raccordement de l’alimentation au microcontrôleur.



![image](docs/sources-documents/images/6_ConnectionAlimentation.jpg)

Raccordement des leds.



![image](docs/sources-documents/images/7_ConnectionLeds.jpg)

Protection du microcontrôleur contre les surintensités de la self du moteur du sapin.



![image](docs/sources-documents/images/8_ProtectionSelfMoteur.jpg)

Raccordement de l’alimentation du moteur du sapin.



![image](docs/sources-documents/images/8_AlimentationEtConnectionMoteurSapin.jpg)

Raccordement des haut parleurs.



![image](docs/sources-documents/images/9_ConnectionHautParleurs.jpg)

Raccordement de l’interrupteur pour couper la musique.



![image](docs/sources-documents/images/10_ConnectionInterrupteurSon.jpg)

## Le programme

Le programme est constitué de 7 fichiers.

Le fichier d’entrée maison-de-noel.ino

```c
#include "Musique.h" // The Music constants
#include "ConfigProject.h" // Configuration of project

void joueMusiqueMonBeauSapin(int);
void joueRythmeMonBeauSapin(int);
void joueMusiqueJinglebells(int);
void joueRythmeJinglebells(int);
bool chenillard_marche(bool);
void lampadere_marche(bool);
void lampadererandom_marche(bool);

unsigned long referencetemps = 0;
unsigned long oldreferencetemps = 0;

// Lampadère
bool lampadere_on = false;
bool lampadererandom_on = false;
bool eclairesapin = false;
// Gestion du temps lampadère
unsigned long tempslampadere = 500;
unsigned long attentelampadere = 0;

// Leds
bool chenillard_on = true;
bool chenillardstick_on = false;
bool chenillardcolor_on = false;
bool chenillardrandom_on = false;
int maxchenillardrandomcount = randomCount;
// Gestion du temps leds
unsigned long tempsleds = 500;
unsigned long attenteleds = 0;

// Sapin
bool animationsapin_on = false;

// Musique
StructDuree noteDuree;
StructDuree rythmeDuree;
int sensorMusiqueOn = 0;
float voltageMusiqueOn = 0;
bool mode_son = false;
bool joue_musique = false;
bool joue_rythme = false;
bool joue_musique_monbeausapin_on = false;
bool joue_rythme_monbeausapin_on = false;
bool musique_monbeausapin_on = false;
bool rythme_monbeausapin_on = false;
bool joue_musique_jinglebells_on = false;
bool joue_rythme_jinglebells_on = false;
bool musique_jinglebells_on = false;
bool rythme_jinglebells_on = false;
unsigned int litmelodie = 0; // Indice lecture notes musiques
// Gestion du temps musique
unsigned long attentemusique = 0;
unsigned long attenterythme = 0;

// Animation musicale Mon beau sapin
bool animation_monbeausapin_on = false;

// Animation visuelle maison
bool animation_visuelle_maison = false;

// Animation musicale Jingle bells
bool animation_jinglebells_on = false;

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
  init_leds();
  init_sapin(moteurSapin, ledsapin);
  init_lampadere(lampadere);
}

// the loop routine runs over and over again forever:
void loop() {
  referencetemps = millis();

  // dépassement de limites de millis() (remise à zéro) TODO à améliorer
  //if (referencetemps<oldreferencetemps) {
  //  attenteleds = 0;
  //  attentelampadere = 0;
  //  attentemusique = 0;
  //}

  // Detection validation musique
  sensorMusiqueOn = analogRead(A0);
  voltageMusiqueOn = sensorMusiqueOn * (5.0 / 1023.0);
  if (voltageMusiqueOn > 2.5) {
    mode_son = true;
  } else {
    mode_son = false;
  }

  // Chenillard de leds
  if (attenteleds<=referencetemps) {
    attenteleds = referencetemps + tempsleds;
    if (chenillard_on) {
      if (!chenillard_marche(chenillard_on)) {
        // Animation suivante
        if (!animation_visuelle_maison) {
         animation_monbeausapin_start();
        }      
      }
    }
    if (animation_visuelle_maison) {
      if (!animation_visuelle_maison_marche(animation_visuelle_maison)) {
        animation_jinglebells_start();
      }
    }
  }

  // Lampadère
  if (attentelampadere<=referencetemps) {
    attentelampadere = referencetemps + tempslampadere;
    if (lampadere_on) {
      lampadere_marche(lampadere_on);
    }
    if (lampadererandom_on) {
      lampadererandom_marche(lampadererandom_on);
    }
  }

  // Mise en marche animation sapin
  animationsapin_marche(animationsapin_on);

  // Mise en marche animation mon beau sapin
  if (animation_monbeausapin_on) {
    if(!animation_monbeausapin_marche(animation_monbeausapin_on)) {
      // Animation suivante
      animation_monbeausapin_stop();
      animation_visuelle_start();
    }
  }
  
  // Mise en marche animation jingle bells
  if (animation_jinglebells_on) {
    if(!animation_jinglebells_marche(animation_jinglebells_on)) {
      // Animation suivante
      animation_jinglebells_stop();
      chenillard_start();
    }
  }
  
  // Mise en marche musique
  if (attentemusique<=referencetemps) {
    if (musique_monbeausapin_on) {
      if(!musique_monbeausapin_marche(musique_monbeausapin_on)) {
        joue_musique = false;
      }
    }
    if (musique_jinglebells_on) {
      chenillardrandom_marche(chenillardrandom_on);
      if(!musique_jinglebells_marche(musique_jinglebells_on)) {
        joue_musique = false;
      }
    }
  }

  // Mise en marche rythme
  if (attenterythme<=referencetemps) {
    if (rythme_monbeausapin_on) {
      if(!rythme_monbeausapin_marche(rythme_monbeausapin_on)) {
        joue_rythme = false;
      }
    }
    if (rythme_jinglebells_on) {
      if(!rythme_jinglebells_marche(rythme_jinglebells_on)) {
        joue_rythme = false;
      }
    }
  }

  //oldreferencetemps=referencetemps;    
}
```

Le fichier de configuration du projet ConfigProject.h

```c
// Définition périphériques
#define ledsapin 2
#define lampadere 3 // the PWM pin the LED is attached to
#define ledRouge1 4 //porte entrée
#define ledRouge2 5 // PWM possible 
#define ledVerte1 6 // PWM possible
#define ledVerte2 7
#define ledbleu1 8
#define moteurSapin 9 // the PWM motor
#define hautparleur1 10 // the PWM HP1
#define hautparleur2 11 // the PWM HP2
#define ledBleu2 12
#define randomCount 100 // Max time random
#define tempoMonbeausapin TEMPO_adagio
#define tempoJingleBells TEMPO_prestissimo
 
```

Le fichier de paramètres musicaux Musique.h

```c
#include <EEPROM.h>

// Définition des fréquences des octaves de musiques
#define OCTAVE_1_DO 33
#define OCTAVE_1_DOd 35
#define OCTAVE_1_RE 37
#define OCTAVE_1_REd 39
#define OCTAVE_1_MI 41
#define OCTAVE_1_FA 44
#define OCTAVE_1_FAd 46
#define OCTAVE_1_SOL 49
#define OCTAVE_1_SOLd 52
#define OCTAVE_1_LA 55
#define OCTAVE_1_SIb 58
#define OCTAVE_1_SI 62
#define OCTAVE_2_DO 65
#define OCTAVE_2_DOd 69
#define OCTAVE_2_RE 73
#define OCTAVE_2_REd 78
#define OCTAVE_2_MI 82
#define OCTAVE_2_FA 87
#define OCTAVE_2_FAd 92
#define OCTAVE_2_SOL 98
#define OCTAVE_2_SOLd 104
#define OCTAVE_2_LA 110
#define OCTAVE_2_SIb 117
#define OCTAVE_2_SI 123
#define OCTAVE_3_DO 131
#define OCTAVE_3_DOd 139
#define OCTAVE_3_RE 157
#define OCTAVE_3_REd 156
#define OCTAVE_3_MI 165
#define OCTAVE_3_FA 175
#define OCTAVE_3_FAd 185
#define OCTAVE_3_SOL 196
#define OCTAVE_3_SOLd 208
#define OCTAVE_3_LA 220
#define OCTAVE_3_SIb 233
#define OCTAVE_3_SI 247
#define OCTAVE_4_DO 262
#define OCTAVE_4_DOd 277
#define OCTAVE_4_RE 294
#define OCTAVE_4_REd 311
#define OCTAVE_4_MI 330
#define OCTAVE_4_FA 349
#define OCTAVE_4_FAd 370
#define OCTAVE_4_SOL 392
#define OCTAVE_4_SOLd 415
#define OCTAVE_4_LA 440
#define OCTAVE_4_SIb 466
#define OCTAVE_4_SI 494
#define OCTAVE_5_DO 523
#define OCTAVE_5_DOd 554
#define OCTAVE_5_RE 587
#define OCTAVE_5_REd 622
#define OCTAVE_5_MI 659
#define OCTAVE_5_FA 698
#define OCTAVE_5_FAd 740
#define OCTAVE_5_SOL 784
#define OCTAVE_5_SOLd 831
#define OCTAVE_5_LA 880
#define OCTAVE_5_SIb 932
#define OCTAVE_5_SI 988
#define OCTAVE_6_DO 1047
#define OCTAVE_6_DOd 1109
#define OCTAVE_6_RE 1175
#define OCTAVE_6_REd 1245
#define OCTAVE_6_MI 1319
#define OCTAVE_6_FA 1397
#define OCTAVE_6_FAd 1480
#define OCTAVE_6_SOL 1568
#define OCTAVE_6_SOLd 1661
#define OCTAVE_6_LA 1760
#define OCTAVE_6_SIb 1865
#define OCTAVE_6_SI 1976
#define OCTAVE_7_DO 2093
#define OCTAVE_7_DOd 2217
#define OCTAVE_7_RE 2349
#define OCTAVE_7_REd 2489
#define OCTAVE_7_MI 2637
#define OCTAVE_7_FA 2794
#define OCTAVE_7_FAd 2960
#define OCTAVE_7_SOL 3136
#define OCTAVE_7_SOLd 3322
#define OCTAVE_7_LA 3520
#define OCTAVE_7_SIb 3729
#define OCTAVE_7_SI 3951
#define OCTAVE_8_DO 4186
#define OCTAVE_8_DOd 4435
#define OCTAVE_8_RE 4699
#define OCTAVE_8_REd 4978
#define OCTAVE_8_MI 5274
#define OCTAVE_8_FA 5588
#define OCTAVE_8_FAd 5920
#define OCTAVE_8_SOL 6272
#define OCTAVE_8_SOLd 6645
#define OCTAVE_8_LA 7040
#define OCTAVE_8_SIb 7459
#define OCTAVE_8_SI 7902
#define OCTAVE_9_DO 8372
#define OCTAVE_9_DOd 8870
#define OCTAVE_9_RE 9397
#define OCTAVE_9_REd 9956
#define OCTAVE_9_MI 10548
#define OCTAVE_9_FA 11175
#define OCTAVE_9_FAd 11840
#define OCTAVE_9_SOL 12544
#define OCTAVE_9_SOLd 13290
#define OCTAVE_9_LA 14080
#define OCTAVE_9_SIb 14917
#define OCTAVE_9_SI 15804

// Silences
#define SILENCE_32eme_soupir 1 // 1/32 de pulsation
#define SILENCE_16eme_soupir 2
#define SILENCE_8eme_soupir 4
#define SILENCE_quart_soupir 8
#define SILENCE_demi_soupir 16
#define SILENCE_soupir 32
#define SILENCE_demi_pause 64
#define SILENCE_pause 128
#define SILENCE_baton_pause 256

// Temps
#define DUREE_quintuple_croche 1 // 1/32 de pulsation
#define DUREE_quadruple_croche 2
#define DUREE_quadruple_croche_pointee 3
#define DUREE_triple_croche 4
#define DUREE_triple_croche_pointee 6
#define DUREE_double_croche 8
#define DUREE_double_croche_pointee 12
#define DUREE_croche 16
#define DUREE_croche_pointee 24
#define DUREE_noire 32
#define DUREE_noire_pointee 48
#define DUREE_blanche 64
#define DUREE_blanche_pointee 96
#define DUREE_ronde 128
#define DUREE_ronde_pointee 192
#define DUREE_carree 256
#define DUREE_carree_pointee 384

// Tempo
#define TEMPO_larghissimo 40 // pulsation par minutes
#define TEMPO_largo 45
#define TEMPO_larghetto 50
#define TEMPO_lentissimo 55
#define TEMPO_lento 60
#define TEMPO_adagiosissimo 65
#define TEMPO_adagissimo 70
#define TEMPO_adagio 75
#define TEMPO_adagietto 80
#define TEMPO_andante 85
#define TEMPO_andantino 90
#define TEMPO_moderato 95
#define TEMPO_moderato2 100
#define TEMPO_allegretto 110
#define TEMPO_allegro 120
#define TEMPO_vivace 130
#define TEMPO_vivace2 140
#define TEMPO_presto 150
#define TEMPO_presto2 160
#define TEMPO_presto3 170
#define TEMPO_prestissimo 180
#define TEMPO_prestissimo2 190
#define TEMPO_prestissimo3 200

typedef struct
{
  bool joue;
  unsigned long temps;
} StructDuree;

typedef struct
{
  unsigned int Duree;
  unsigned int Octave;
  unsigned int Nuance;
} StructNote;

typedef struct
{
  unsigned int Duree;
  unsigned int Frequence;
} StructRythme;

// Melodie à jouer
static StructNote MonBeauSapin[] = {
  {DUREE_noire, OCTAVE_3_DO, 1},
  {DUREE_croche_pointee, OCTAVE_3_FA, 2},
  {DUREE_double_croche, OCTAVE_3_FA, 3},
  {DUREE_blanche, OCTAVE_3_FA, 4},
  {DUREE_noire, OCTAVE_3_SOL, 5},
  {DUREE_croche_pointee, OCTAVE_3_LA, 6},
  {DUREE_double_croche, OCTAVE_3_LA, 7},
  {DUREE_noire_pointee, OCTAVE_3_LA, 8},
  {DUREE_croche, OCTAVE_3_FA, 9},
  {DUREE_croche, OCTAVE_3_SOL, 10},
  {DUREE_croche, OCTAVE_3_LA, 11},
  {DUREE_noire, OCTAVE_3_SI, 12},
  {DUREE_noire, OCTAVE_3_MI, 13},
  {DUREE_noire, OCTAVE_3_SOL, 14},
  {DUREE_noire, OCTAVE_3_FA, 15},
  {SILENCE_demi_soupir, 0, 0},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_noire_pointee, OCTAVE_4_RE, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_4_DO, 16},
  {DUREE_noire, OCTAVE_3_SI, 16},
  {SILENCE_demi_soupir, 0, 0},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_4_DO, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_SI, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_blanche, OCTAVE_3_LA, 16},
  {DUREE_noire, OCTAVE_3_DO, 16},
  {DUREE_croche_pointee, OCTAVE_3_FA, 16},
  {DUREE_double_croche, OCTAVE_3_FA, 16},
  {DUREE_blanche, OCTAVE_3_FA, 16},
  {DUREE_noire, OCTAVE_3_SOL, 16},
  {DUREE_croche_pointee, OCTAVE_3_LA, 16},
  {DUREE_double_croche, OCTAVE_3_LA, 16},
  {DUREE_noire_pointee, OCTAVE_3_LA, 16},
  {DUREE_croche, OCTAVE_3_FA, 16},
  {DUREE_croche, OCTAVE_3_SOL, 16},
  {DUREE_croche, OCTAVE_3_LA, 16},
  {DUREE_noire, OCTAVE_3_SI, 16},
  {DUREE_noire, OCTAVE_3_MI, 16},
  {DUREE_noire, OCTAVE_3_SOL, 16},
  {DUREE_noire, OCTAVE_3_FA, 16},
};

// Rythme
static StructRythme RythmeMonBeauSapin[] {
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_1_SI},
  {DUREE_blanche, OCTAVE_2_DO},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_DO},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_blanche, OCTAVE_2_MI},
  {DUREE_blanche, OCTAVE_1_LA},
  {DUREE_noire, OCTAVE_2_MI},
  {DUREE_noire, OCTAVE_1_LA},
  {DUREE_noire, OCTAVE_2_MI},
  {SILENCE_demi_pause, 0},
};

// Melodie à jouer
static StructNote JingleBells[] = {
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_DO, 16},
  {SILENCE_pause, 0, 0},
  {SILENCE_soupir, 0, 0},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_LA, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {SILENCE_pause, 0, 0},
  //
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_6_DO, 16},
  {DUREE_croche, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire_pointee, OCTAVE_6_FA, 16},
  {DUREE_croche, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_RE, 16},
  {DUREE_blanche, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_blanche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire_pointee, OCTAVE_6_DO, 16},
  {DUREE_croche, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire_pointee, OCTAVE_6_FA, 16},
  {DUREE_croche, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_croche, OCTAVE_6_MI, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_SOL, 16},
  {DUREE_noire, OCTAVE_6_FA, 16},
  {DUREE_noire, OCTAVE_6_RE, 16},
  {DUREE_ronde, OCTAVE_6_DO, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire_pointee, OCTAVE_6_DO, 16},
  //{DUREE_croche, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire_pointee, OCTAVE_6_FA, 16},
  //{DUREE_croche, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_RE, 16},
  //{DUREE_blanche, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_blanche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire_pointee, OCTAVE_6_DO, 16},
  //{DUREE_croche, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire_pointee, OCTAVE_6_FA, 16},
  //{DUREE_croche, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_croche, OCTAVE_6_MI, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_SOL, 16},
  //{DUREE_noire, OCTAVE_6_FA, 16},
  //{DUREE_noire, OCTAVE_6_RE, 16},
  //{DUREE_ronde, OCTAVE_6_DO, 16},
};

// Rythme
static StructRythme RythmeJingleBells[] {
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SOL},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_LA},
  {DUREE_noire, OCTAVE_3_LA},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SI},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_SOL},
  {DUREE_noire, OCTAVE_3_SOL},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {DUREE_ronde, OCTAVE_3_LA},
  {DUREE_noire, OCTAVE_3_LA},
  {SILENCE_soupir, 0},
  {SILENCE_demi_pause, 0},
  {SILENCE_pause, 0},
  {SILENCE_pause, 0},
  {DUREE_ronde, OCTAVE_4_DO},
  //
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
  //{SILENCE_pause, 0},
};
```

Le fichier des musiques de Noël et de leurs contrôles MusiquesNoel.ino

```c
#include "Volume3.h" // The Volume songs library

unsigned int litrythme = 0; 
unsigned long pulsemusique = 0;

// Tempo à jouer
void setTempoMusique(unsigned long tempo) {
  pulsemusique = 1800/tempo;
}

// Melodie à jouer
// Mon beau sapin
void joueMusiqueMonBeauSapin(int hautparleur, unsigned int note) {
  if (mode_son) {
    vol.tone(hautparleur, MonBeauSapin[note].Octave, MonBeauSapin[note].Nuance*64);
  } else {
    vol.tone(hautparleur, 0, 0);
  }
}

StructDuree joue_musique_monbeausapin(bool marche) {
  StructDuree resultat;
  resultat.temps=MonBeauSapin[litmelodie].Duree*pulsemusique;
  if (marche) {
    joueMusiqueMonBeauSapin(hautparleur1, litmelodie);
  } else {
    joue_musique_monbeausapin_stop();
  }
  litmelodie++;
  if (litmelodie>sizeof(MonBeauSapin)/sizeof(StructNote)) {
    joue_musique_monbeausapin_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_musique_monbeausapin_stop() {
  Serial.println("Musique Fin");
  vol.noTone();
  litmelodie = 0;
  joue_musique_monbeausapin_on = false;
}

void joue_musique_monbeausapin_start() {
  Serial.println("Musique");
  joue_musique_monbeausapin_on = true;
}

bool musique_monbeausapin_marche(bool marche) {
  setTempoMusique(tempoMonbeausapin);
  noteDuree = joue_musique_monbeausapin(musique_monbeausapin_on);
  attentemusique = referencetemps + noteDuree.temps;
  if(!noteDuree.joue) {
    musique_monbeausapin_stop();
    return false;
  }
  return marche;
}

void musique_monbeausapin_stop() {
  Serial.println("Musique MonBeauSapin Fin");
  attentemusique = 0;
  musique_monbeausapin_on = false;
}

void musique_monbeausapin_start() {
  Serial.println("Musique MonBeauSapin");
  joue_musique_monbeausapin_start();
  musique_monbeausapin_on = true;
}

// Rythme à jouer
void joueRythmeMonBeauSapin(int hautparleur, unsigned int note) {
  if (RythmeMonBeauSapin[note].Frequence!=0) {
    tone(hautparleur, RythmeMonBeauSapin[note].Frequence, RythmeMonBeauSapin[note].Duree);
  } else {
    tone(hautparleur, 0, 0);
  }
}

StructDuree joue_rythme_monbeausapin(bool marche) {
  StructDuree resultat;
  resultat.temps=RythmeMonBeauSapin[litrythme].Duree*pulsemusique;
  if (marche && mode_son) {
    joueRythmeMonBeauSapin(hautparleur2, litrythme);
  } else {
    joue_rythme_monbeausapin_stop();
  }
  litrythme++;
  if (litrythme>sizeof(RythmeMonBeauSapin)/sizeof(StructRythme)) {
    joue_rythme_monbeausapin_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_rythme_monbeausapin_stop() {
  Serial.println("Rythme Fin");
  noTone(hautparleur2);
  litrythme = 0;
  joue_rythme_monbeausapin_on = false;
}

void joue_rythme_monbeausapin_start() {
  Serial.println("Rythme");
  joue_rythme_monbeausapin_on = true;
}

bool rythme_monbeausapin_marche(bool marche) {
      rythmeDuree = joue_rythme_monbeausapin(rythme_monbeausapin_on);
      attenterythme = referencetemps + rythmeDuree.temps;
      if (!rythmeDuree.joue) {
        rythme_monbeausapin_stop();
        return false;
      }
      return marche;
}

void rythme_monbeausapin_stop() {
  Serial.println("Rythme MonBeauSapin Fin");
  joue_rythme_monbeausapin_stop();
  attenterythme = 0;
  rythme_monbeausapin_on = false;
}

void rythme_monbeausapin_start() {
  Serial.println("Rythme MonBeauSapin");
  joue_rythme_monbeausapin_start();
  rythme_monbeausapin_on = true;
}

// Jingle bells
void joueMusiqueJingleBells(int hautparleur, unsigned int note) {
  if (mode_son) {
    vol.tone(hautparleur, JingleBells[note].Octave, JingleBells[note].Nuance*64);
  } else {
    vol.tone(hautparleur, 0, 0);
  }
}

StructDuree joue_musique_jinglebells(bool marche) {
  StructDuree resultat;
  resultat.temps=JingleBells[litmelodie].Duree*pulsemusique;
  if (marche) {
    joueMusiqueJingleBells(hautparleur1, litmelodie);
  } else {
    joue_musique_jinglebells_stop();
  }
  litmelodie++;
  if (litmelodie>sizeof(JingleBells)/sizeof(StructNote)) {
    joue_musique_jinglebells_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_musique_jinglebells_stop() {
  Serial.println("Musique Fin");
  vol.noTone();
  litmelodie = 0;
  joue_musique_jinglebells_on = false;
}

void joue_musique_jinglebells_start() {
  Serial.println("Musique");
  joue_musique_jinglebells_on = true;
}

bool musique_jinglebells_marche(bool marche) {
  setTempoMusique(tempoJingleBells);
  noteDuree = joue_musique_jinglebells(musique_jinglebells_on);
  attentemusique = referencetemps + noteDuree.temps;
  if(!noteDuree.joue) {
    musique_jinglebells_stop();
    return false;
  }
  return marche;
}

void musique_jinglebells_stop() {
  Serial.println("Musique Jinglebells Fin");
  attentemusique = 0;
  musique_jinglebells_on = false;
}

void musique_jinglebells_start() {
  Serial.println("Musique Jinglebells");
  joue_musique_jinglebells_start();
  musique_jinglebells_on = true;
}

// Rythme à jouer
void joueRythmeJingleBells(int hautparleur, unsigned int note) {
  if (RythmeJingleBells[note].Frequence!=0) {
    tone(hautparleur, RythmeJingleBells[note].Frequence, RythmeJingleBells[note].Duree);
  } else {
    tone(hautparleur, 0, 0);
  }
}

StructDuree joue_rythme_jinglebells(bool marche) {
  StructDuree resultat;
  resultat.temps=RythmeJingleBells[litrythme].Duree*pulsemusique;
  if (marche && mode_son) {
    joueRythmeJingleBells(hautparleur2, litrythme);
  } else {
    joue_rythme_jinglebells_stop();
  }
  litrythme++;
  if (litrythme>sizeof(RythmeJingleBells)/sizeof(StructRythme)) {
    joue_rythme_jinglebells_stop();
    resultat.joue=false;
    return resultat;
  } else {
    resultat.joue=true;
    return resultat;
  }
}

void joue_rythme_jinglebells_stop() {
  Serial.println("Rythme Fin");
  noTone(hautparleur2);
  litrythme = 0;
  joue_rythme_jinglebells_on = false;
}

void joue_rythme_jinglebells_start() {
  Serial.println("Rythme");
  joue_rythme_jinglebells_on = true;
}

bool rythme_jinglebells_marche(bool marche) {
      rythmeDuree = joue_rythme_jinglebells(rythme_jinglebells_on);
      attenterythme = referencetemps + rythmeDuree.temps;
      if (!rythmeDuree.joue) {
        rythme_jinglebells_stop();
        return false;
      }
      return marche;
}

void rythme_jinglebells_stop() {
  Serial.println("Rythme Jinglebells Fin");
  joue_rythme_jinglebells_stop();
  attenterythme = 0;
  rythme_jinglebells_on = false;
}

void rythme_jinglebells_start() {
  Serial.println("Rythme Jinglebells");
  joue_rythme_jinglebells_start();
  rythme_jinglebells_on = true;
}
```

Le fichier de contrôle du sapin Sapin.ino

```c
bool sapin_on = false;

void init_sapin(int moteur_sapin, int led_sapin) {
  // declare pin to be an output:
  pinMode(led_sapin, OUTPUT);
  pinMode(moteurSapin, OUTPUT);

  ledsapin_stop();
  moteursapin_stop();
}

void moteursapin_marche(bool marche) {
  if(marche && sapin_on) {
    digitalWrite(moteurSapin, LOW);
  } else {
    digitalWrite(moteurSapin, HIGH); 
  }  
}

void moteursapin_stop() {
  Serial.println("Moteur Sapin Fin");
  digitalWrite(moteurSapin, HIGH);
  sapin_on = false;
}

void moteursapin_start() {
  Serial.println("Moteur Sapin");
  sapin_on = true;  
}

void animationsapin_marche(bool marche) {
  ledsapin_marche(marche);
  moteursapin_marche(marche);
}
```

Le fichier de commandes des éclairages Eclairages.ino

```c
int brightness = 0;    // how bright the LED is
int fadeAmount = 1;    // how many points to fade the LED by
int chenillard = 0;
int chenillardstick = 0;
int chenillardcolor = 0;
int chenillardrandom = 0;
int chenillardrandomcount = 0;
bool flag_chenillard = false;

void init_leds() {
  // declare pin to be an output:
  pinMode(ledRouge1, OUTPUT);
  pinMode(ledRouge2, OUTPUT);
  pinMode(ledVerte1, OUTPUT);
  pinMode(ledVerte2, OUTPUT);
  pinMode(ledbleu1, OUTPUT);
  pinMode(ledBleu2, OUTPUT);

  digitalWrite(ledRouge1, LOW);
  digitalWrite(ledRouge2, LOW);
  digitalWrite(ledVerte1, LOW);
  digitalWrite(ledVerte2, LOW);
  digitalWrite(ledbleu1, LOW);
  digitalWrite(ledBleu2, LOW);  
}

void init_lampadere(int led_lampadere) {
  // declare pin to be an output:
  pinMode(led_lampadere, OUTPUT);
  lampadere_stop();   
}

void led_on(int number) {
  switch(number) {
    case 1:
      digitalWrite(ledRouge1, HIGH);
      break;
    case 2:
      digitalWrite(ledVerte1, HIGH);
      break;
    case 3:
      digitalWrite(ledbleu1, HIGH);
      break;   
    case 4:
      digitalWrite(ledRouge2, HIGH);
      break;
    case 5:
      digitalWrite(ledVerte2, HIGH);   
      break;
    case 6:
      digitalWrite(ledBleu2, HIGH); 
      break;
  }  
}

void led_off(int number) {
  switch(number) {
    case 1:
      digitalWrite(ledRouge1, LOW);
      break;
    case 2:
      digitalWrite(ledVerte1, LOW);
      break;
    case 3:
      digitalWrite(ledbleu1, LOW);
      break;   
    case 4:
      digitalWrite(ledRouge2, LOW);
      break;
    case 5:
      digitalWrite(ledVerte2, LOW);   
      break;
    case 6:
      digitalWrite(ledBleu2, LOW); 
      break;
  }  
}

void ledsapin_marche(bool marche) {
  if (marche) {
    digitalWrite(ledsapin, HIGH);
  } else {
    digitalWrite(ledsapin, LOW);
  }
}

void ledsapin_stop() {
  Serial.println("Led Sapin Fin");
  digitalWrite(ledsapin, LOW);
  eclairesapin = false;
}

void ledsapin_start() {
  Serial.println("Led Sapin");
  eclairesapin = true;
}

bool chenillard_marche(bool marche) {
  if (marche) {
    if (chenillard!=0) {
      led_off(chenillard);
    }
    chenillard = chenillard + 1;
    if (chenillard>=7){
      chenillard_stop();
      return false;
    } else {
      led_on(chenillard);
    }
    return true;
  }
}

void chenillard_stop() {
  Serial.println("Chenillard Fin");
  chenillard = 0;
  chenillard_on = false;
}

void chenillard_start() {
  Serial.println("Chenillard");
  chenillard_on = true;
}

bool chenillardstick_marche(bool marche) {
  if (!flag_chenillard) {
    chenillardstick = chenillardstick + 1;
    if (chenillardstick>=7){
      flag_chenillard = true;
      return true;
    } else {
      led_on(chenillardstick);
      return true;
    }
  } else {
    chenillardstick = chenillardstick - 1;
    if (chenillardstick<=0){
      chenillardstick_stop();
      return false;
    } else {
      led_off(chenillardstick);
      return true;
    }
  }
}

void chenillardstick_stop() {
  Serial.println("Chenillardstick Fin");
  flag_chenillard = false;
  chenillardstick_on = false;
}

void chenillardstick_start() {
  Serial.println("Chenillardstick");
  chenillardstick_on = true;
}

bool chenillardcolor_marche(bool marche) {
  if (marche) {
    if(chenillardcolor!=0){
      led_off(chenillardcolor);
      led_off(chenillardcolor + 3);
    }
    chenillardcolor = chenillardcolor + 1;
    if (chenillardcolor>=4){
      chenillardcolor_stop();
      return false;        
    } else {
      led_on(chenillardcolor);
      led_on(chenillardcolor + 3);
    }
  }
  return true; 
}

void chenillardcolor_stop() {
  Serial.println("Chenillardcolor Fin");
  chenillardcolor = 0;
  chenillardcolor_on = false;
}

void chenillardcolor_start() {
  Serial.println("Chenillardcolor");
  chenillardcolor_on = true;
}

bool chenillardrandom_marche(bool marche) {
  if (marche) {
    if (chenillardrandom!=0) {
      led_off(chenillardrandom);
    }
    chenillardrandom = random(1,6);
    chenillardrandomcount = chenillardrandomcount + 1;
    if (chenillardrandomcount>=maxchenillardrandomcount){
      chenillardrandom_stop();
      return false;
    } else {
      led_on(chenillardrandom);
      return true;
    }
  }
}

void chenillardrandom_stop() {
  Serial.println("Chenillardrandom Fin");
  chenillardrandom = 0;
  chenillardrandomcount = 0;
  chenillardrandom_on = false;
}

void chenillardrandom_start() {
  Serial.println("Chenillardrandom");
  chenillardrandom_on = true;
}

void lampadere_shine(int led_lampadere) {
  // set the brightness of pin 9:
  if (lampadere_on) {
    analogWrite(led_lampadere, brightness);
  }

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
}

void lampadere_marche(bool marche) {
  //Serial.println("Lampadere");
  if (marche) {
    lampadere_shine(lampadere);
  } else {
    lampadere_stop();
  }  
}

void lampadere_stop() {
  Serial.println("Lampadere Fin");
  analogWrite(lampadere, 0);
  brightness = 0;
  lampadere_on = false;
}

void lampadere_start() {
  Serial.println("Lampadere");
  lampadere_on = true;
}

void lampadere_random(int led_lampadere) {
  if (lampadererandom_on) {
    analogWrite(led_lampadere, random(255));
  }
}

void lampadererandom_marche(bool marche) {
  if (marche) {
    lampadere_random(lampadere);
  } else {
    lampadere_stop();
  }  
}

void lampadererandom_stop() {
  Serial.println("Lampadere random Fin");
  analogWrite(lampadere, 0);
  lampadererandom_on = false;
}

void lampadererandom_start() {
  Serial.println("Lampadere random");
  lampadererandom_on = true;
}

bool animation_visuelle_maison_marche(bool marche) {
  if (chenillardstick_on) {
    if (!chenillardstick_marche(chenillardstick_on)) {
      chenillardcolor_start();
    }
  }
  if (chenillardcolor_on) {
    if (!chenillardcolor_marche(chenillardcolor_on)) {
      chenillardrandom_start();
    }
  }
  if (chenillardrandom_on) {
    if (!chenillardrandom_marche(chenillardrandom_on)) {
      animation_visuelle_stop();
      return false;
    }
  }
  return true;
}
```

Le fichiers des animations du village de Noël Animations.ino

```c
void animationsapin_stop() {
  Serial.println("Animation Sapin Fin");
  ledsapin_stop();
  moteursapin_stop();
  animationsapin_on = false;
}

void animationsapin_start() {
  Serial.println("Animation Sapin");
  ledsapin_start();
  moteursapin_start();
  animationsapin_on = true;
}

bool animation_monbeausapin_marche(bool marche) {
  if(!joue_musique) {
    return false;
  }
  return marche;
}

bool animation_jinglebells_marche(bool marche) {
  if(!joue_musique) {
    return false;
  }
  return marche;
}

void animation_visuelle_stop() {
  Serial.println("Animation visuelle Fin");
  //lampadererandom_stop();
  animationsapin_stop();
  animation_visuelle_maison = false;
}

void animation_visuelle_start() {
  Serial.println("Animation visuelle");
  //lampadererandom_start();
  animationsapin_start();
  chenillardstick_start();
  animation_visuelle_maison = true;  
}

void animation_monbeausapin_stop() {
  Serial.println("Animation MonBeauSapin Fin");
  lampadere_stop();
  animationsapin_stop();
  musique_monbeausapin_stop();
  rythme_monbeausapin_stop();
  animation_monbeausapin_on = false;
}

void animation_monbeausapin_start() {
  Serial.println("Animation MonBeauSapin");
  lampadere_start();
  animationsapin_start();
  musique_monbeausapin_start();
  rythme_monbeausapin_start();
  animation_monbeausapin_on = true;
  joue_musique = true;
  joue_rythme = true;
}

void animation_jinglebells_stop() {
  Serial.println("Animation Jinglebells Fin");
  chenillardrandom_stop();
  musique_jinglebells_stop();
  rythme_jinglebells_stop();
  animation_jinglebells_on = false;
}

void animation_jinglebells_start() {
  Serial.println("Animation Jinglebells");
  chenillardrandom_start();
  musique_jinglebells_start();
  rythme_jinglebells_start();
  animation_jinglebells_on = true;
  joue_musique = true;
  joue_rythme = true; 
}
```

## Améliorations possibles

La mémoire limitée de l’Arduino Nano limite les animations possibles.

Le montage est prévu pour pouvoir évoluer avec une EEPROM en i2c sur les pattes A4 et A5 de l’Arduino Nano.

Les musiques peuvent alors être stockées dans une EEPROM, ce qui permettrait de récupérer pas mal de ressources pour le microcontrôleur. Il faut alors modifier ce programme pour lire les musiques à partir de l’EEPROM, et créer un autre programme pour écrire les musiques dans une EEPROM.

Peut-être une V2 pour l’année prochaine ?

## Le résultat final

[Voir sur Youtube](https://youtu.be/Mbh5EpDlPqY)
