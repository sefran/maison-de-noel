int brightness = 0;    // how bright the LED is
int fadeAmount = 1;    // how many points to fade the LED by
int chenillard = 0;
int chenillardstick = 0;
int chenillardcolor = 0;
int chenillardrandom = 0;
int chenillardrandomcount = 0;
bool flag_chenillard = false;

void init_leds() {
  // declare pin to be an output:
  pinMode(ledRouge1, OUTPUT);
  pinMode(ledRouge2, OUTPUT);
  pinMode(ledVerte1, OUTPUT);
  pinMode(ledVerte2, OUTPUT);
  pinMode(ledbleu1, OUTPUT);
  pinMode(ledBleu2, OUTPUT);

  digitalWrite(ledRouge1, LOW);
  digitalWrite(ledRouge2, LOW);
  digitalWrite(ledVerte1, LOW);
  digitalWrite(ledVerte2, LOW);
  digitalWrite(ledbleu1, LOW);
  digitalWrite(ledBleu2, LOW);  
}

void init_lampadere(int led_lampadere) {
  // declare pin to be an output:
  pinMode(led_lampadere, OUTPUT);
  lampadere_stop();   
}

void led_on(int number) {
  switch(number) {
    case 1:
      digitalWrite(ledRouge1, HIGH);
      break;
    case 2:
      digitalWrite(ledVerte1, HIGH);
      break;
    case 3:
      digitalWrite(ledbleu1, HIGH);
      break;   
    case 4:
      digitalWrite(ledRouge2, HIGH);
      break;
    case 5:
      digitalWrite(ledVerte2, HIGH);   
      break;
    case 6:
      digitalWrite(ledBleu2, HIGH); 
      break;
  }  
}

void led_off(int number) {
  switch(number) {
    case 1:
      digitalWrite(ledRouge1, LOW);
      break;
    case 2:
      digitalWrite(ledVerte1, LOW);
      break;
    case 3:
      digitalWrite(ledbleu1, LOW);
      break;   
    case 4:
      digitalWrite(ledRouge2, LOW);
      break;
    case 5:
      digitalWrite(ledVerte2, LOW);   
      break;
    case 6:
      digitalWrite(ledBleu2, LOW); 
      break;
  }  
}

void ledsapin_marche(bool marche) {
  if (marche) {
    digitalWrite(ledsapin, HIGH);
  } else {
    digitalWrite(ledsapin, LOW);
  }
}

void ledsapin_stop() {
  Serial.println("Led Sapin Fin");
  digitalWrite(ledsapin, LOW);
  eclairesapin = false;
}

void ledsapin_start() {
  Serial.println("Led Sapin");
  eclairesapin = true;
}

bool chenillard_marche(bool marche) {
  if (marche) {
    if (chenillard!=0) {
      led_off(chenillard);
    }
    chenillard = chenillard + 1;
    if (chenillard>=7){
      chenillard_stop();
      return false;
    } else {
      led_on(chenillard);
    }
    return true;
  }
}

void chenillard_stop() {
  Serial.println("Chenillard Fin");
  chenillard = 0;
  chenillard_on = false;
}

void chenillard_start() {
  Serial.println("Chenillard");
  chenillard_on = true;
}

bool chenillardstick_marche(bool marche) {
  if (!flag_chenillard) {
    chenillardstick = chenillardstick + 1;
    if (chenillardstick>=7){
      flag_chenillard = true;
      return true;
    } else {
      led_on(chenillardstick);
      return true;
    }
  } else {
    chenillardstick = chenillardstick - 1;
    if (chenillardstick<=0){
      chenillardstick_stop();
      return false;
    } else {
      led_off(chenillardstick);
      return true;
    }
  }
}

void chenillardstick_stop() {
  Serial.println("Chenillardstick Fin");
  flag_chenillard = false;
  chenillardstick_on = false;
}

void chenillardstick_start() {
  Serial.println("Chenillardstick");
  chenillardstick_on = true;
}

bool chenillardcolor_marche(bool marche) {
  if (marche) {
    if(chenillardcolor!=0){
      led_off(chenillardcolor);
      led_off(chenillardcolor + 3);
    }
    chenillardcolor = chenillardcolor + 1;
    if (chenillardcolor>=4){
      chenillardcolor_stop();
      return false;        
    } else {
      led_on(chenillardcolor);
      led_on(chenillardcolor + 3);
    }
  }
  return true; 
}

void chenillardcolor_stop() {
  Serial.println("Chenillardcolor Fin");
  chenillardcolor = 0;
  chenillardcolor_on = false;
}

void chenillardcolor_start() {
  Serial.println("Chenillardcolor");
  chenillardcolor_on = true;
}

bool chenillardrandom_marche(bool marche) {
  if (marche) {
    if (chenillardrandom!=0) {
      led_off(chenillardrandom);
    }
    chenillardrandom = random(1,6);
    chenillardrandomcount = chenillardrandomcount + 1;
    if (chenillardrandomcount>=maxchenillardrandomcount){
      chenillardrandom_stop();
      return false;
    } else {
      led_on(chenillardrandom);
      return true;
    }
  }
}

void chenillardrandom_stop() {
  Serial.println("Chenillardrandom Fin");
  chenillardrandom = 0;
  chenillardrandomcount = 0;
  chenillardrandom_on = false;
}

void chenillardrandom_start() {
  Serial.println("Chenillardrandom");
  chenillardrandom_on = true;
}

void lampadere_shine(int led_lampadere) {
  // set the brightness of pin 9:
  if (lampadere_on) {
    analogWrite(led_lampadere, brightness);
  }

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
}

void lampadere_marche(bool marche) {
  //Serial.println("Lampadere");
  if (marche) {
    lampadere_shine(lampadere);
  } else {
    lampadere_stop();
  }  
}

void lampadere_stop() {
  Serial.println("Lampadere Fin");
  analogWrite(lampadere, 0);
  brightness = 0;
  lampadere_on = false;
}

void lampadere_start() {
  Serial.println("Lampadere");
  lampadere_on = true;
}

void lampadere_random(int led_lampadere) {
  if (lampadererandom_on) {
    analogWrite(led_lampadere, random(255));
  }
}

void lampadererandom_marche(bool marche) {
  if (marche) {
    lampadere_random(lampadere);
  } else {
    lampadere_stop();
  }  
}

void lampadererandom_stop() {
  Serial.println("Lampadere random Fin");
  analogWrite(lampadere, 0);
  lampadererandom_on = false;
}

void lampadererandom_start() {
  Serial.println("Lampadere random");
  lampadererandom_on = true;
}

bool animation_visuelle_maison_marche(bool marche) {
  if (chenillardstick_on) {
    if (!chenillardstick_marche(chenillardstick_on)) {
      chenillardcolor_start();
    }
  }
  if (chenillardcolor_on) {
    if (!chenillardcolor_marche(chenillardcolor_on)) {
      chenillardrandom_start();
    }
  }
  if (chenillardrandom_on) {
    if (!chenillardrandom_marche(chenillardrandom_on)) {
      animation_visuelle_stop();
      return false;
    }
  }
  return true;
}
